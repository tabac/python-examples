import json
import datetime

class Person:
    def __init__(self, name, email, tel, birthday):
        self.name = name
        self.email = email
        self.tel = tel
        self.birthday = birthday

    def __str__(self):
        s = "Person({}, {}, {}, {})".format(
            self.name, self.email, self.tel, self.birthday
        )
        return s

    def column(self):
        return "{:20} {:20} {:10} {:10}".format(
                self.name, self.email, self.tel, self.birthday
            )

    def to_dict(self):
        d = {}
        d['name'] = self.name
        d['email'] = self.email
        d['tel'] = self.tel
        d['birthday'] = self.birthday
        return d        


class PersonList:
    def __init__(self):
        self._list = []

    def column(self):
        s = ""
        for pers in self._list:
            s = s + pers.column() + "\n"
        return s

    def insert(self, pers):
        self._list.append(pers)

    def fulllist(self):
        return self._list[:]

    def find(self, search):
        for pers in self._list:
            if search in pers.name:
                return pers

    def delete(self, p):
        try:
            self._list.remove(p)
            return p
        except ValueError:
            return None


if __name__ == "__main__":
    data = PersonList()
    data.insert(Person(
        name="Carl Otto", email="carl@corp.com", 
        tel="11223344", birthday="1991-10-02"
    ))

    data.insert(Person(
        name="Peter Anderson", email="peter@corp.com", 
        tel="44332211", birthday="1983-11-09"
    ))
   
    for p in data.fulllist():
        print(p)

    p = data.find("Carl")
    print(p)
    p.tel = "88885555"
    print(p)
    data.delete(p)

    for p in data.fulllist():
        print(p)
