class Person:
    def __init__(self, name, email, tel, birthday):
        pass

    def __str__(self):
        pass

    def column(self):
        pass

    def to_dict(self):
        pass


class PersonList:
    def __init__(self):
        pass

    def column(self):
        pass

    def insert(self, pers):
        pass

    def fulllist(self):
        pass

    def find(self, search):
        pass

    def delete(self, p):
        pass


if __name__ == "__main__":
    data = PersonList()
    data.insert(Person(
        name="Carl Otto", email="carl@corp.com", 
        tel="11223344", birthday="1991-10-02"
    ))

    data.insert(Person(
        name="Peter Anderson", email="peter@corp.com", 
        tel="44332211", birthday="1983-11-09"
    ))
   
    for p in data.fulllist():
        print(p)

    p = data.find("Carl")
    print(p)
    p.tel = "88885555"
    print(p)
    data.delete(p)

    for p in data.fulllist():
        print(p)
