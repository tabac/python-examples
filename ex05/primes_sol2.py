import math

def primes(limit):
    candlist = list(range(3, limit+1, 2))
    primelist = [2]
    sq = int(math.sqrt(limit))
    while candlist:
        p = candlist.pop(0)
        primelist.append(p)
        if p > sq:
            primelist.extend(candlist)
            break
        candlist = [i for i in candlist if i % p != 0]
        # for i in candlist[:]:
        #     if i % p == 0:
        #         candlist.remove(i)
    return primelist

if __name__ == "__main__":
    print(primes(1000))
