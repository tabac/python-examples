def primes(limit):
    candlist = list(range(2, limit+1))
    primelist = []
    while candlist:
        p = candlist.pop(0)
        primelist.append(p)
        candlist = [i for i in candlist if i % p != 0]
    return primelist

if __name__ == "__main__":
    print(primes(1000))
