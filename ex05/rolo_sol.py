data = [
    {
        "name": "Adrian Smidth",
        "email": "adrian@aol.com",
        "tel": "11223344",
        "birthday": "1970-11-06"
    },
    {
        "name": "Ben Forrest",
        "email": "ben@yahoo.com",
        "tel": "22334455",
        "birthday": "1969-09-26"
    },
    {
        "name": "Charlie Wilson",
        "email": "charlie@gmail.com",
        "tel": "44332211",
        "birthday": "1983-03-17"
    },
]

def insert_person(data, name, email, tel, birthday):
    p = {}
    p['name'] = name
    p['email'] = email
    p['tel'] = tel
    p['birthday'] = birthday
    data.append(p)

def list_persons(data):
    for p in data:
        print("{:20} {:20} {:10} {:10}".format(
                p["name"], p["email"], p["tel"], p["birthday"]
            ))

def find_person(data, search):
    for p in data:
        if search in p["name"]:
            return p

def update_person(data, search, 
        name="", email="", tel="", birthday=""):
    for p in data:
        if search in p["name"]:
            if name:
                p["name"] = name
            if email:
                p["email"] = email
            if tel:
                p["tel"] = tel
            if birthday:
                p["birthday"] = birthday

def delete_person(data, search):
    for p in data:
        if search in p["name"]:
            data.remove(p)
            break

if __name__ == "__main__":
    list_persons(data)
    print("----")
    insert_person(data, "Dennis Rover", "dennis@ofir.dk", "66778899", "1987-06-05")
    list_persons(data)
    print("----")
    update_person(data, "Rover", email="dennis@email.dk")
    delete_person(data, "Ben")
    list_persons(data)
    print("----")
