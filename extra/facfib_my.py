import doctest

def fac(n):
    """Factorial function

    >>> fac(0)
    1
    >>> fac(5)
    120
    """
    prod = 1
    for i in range(1, n+1):
        prod = prod * i
    return prod

_cache = {}
def fib(n):
    """The fibonacci function

    >>> fib(6)
    8
    >>> fib(7)
    13
    """
    if n in _cache:
        return _cache[n]
    elif n < 2:
        _cache[n] = n
        return n
    else:
        _cache[n] = fib(n-1) + fib(n-2)
        return _cache[n]

if __name__ == "__main__":
    doctest.testmod()
