import rolo01
import json

data = []

menu = """
    1. List contacts
    2. Enter new contact
    3. Find contacts
    4. Update contacts
    5. Delete contacts
    6. Read contact file
    7. Write contact file

    0. Exit
"""
try:
    while True:
        print(menu)
        choice = input("Enter choice : ")
        if choice == "1":
            output = rolo01.list_contacts(data)
            print(output)
        elif choice == "2":
            name = input("Name ......... : ")
            email = input("Email ........ : ")
            tel = input("Tel .......... : ")
            birthday = input("Birthday ..... : ")
            rolo01.insert_contact(data, name, email, tel, birthday)
        elif choice == "3":
            search_name = input("Name to find : ")
            persons = rolo01.find_contact(data, search_name)
            output = rolo01.list_contacts(persons)
            print(output)
        elif choice == "4":
            search_name = input("Name to update : ")
            if name == '':
                name = None
            if email == '':
                email = None
            if tel == '':
                tel = None
            if birthday == '':
                birthday = None
            rolo01.update_contact(data, search_name, name, email, tel, birthday)
        elif choice == "5":
            search_name = input("Name to delete : ")
            rolo01.delete_contact(data, search_name)
        elif choice == "6":
            try:
                filename = input("Enter name of contact file (JSON) : ")
                rolo01.jsonread_contact(data, filename)
            except OSError as e:
                print("The file could not be opened : ", e)
            except json.JSONDecodeError as e:
                print("Illformed JSON file : ", e)
        elif choice == "7":
            try:
                filename = input("Enter name of contact file (JSON) : ")
                rolo01.jsonwrite_contact(data, filename)
            except OSError as e:
                print("The file could not be written : ", e)
        elif choice == "0":
            break
except Exception as e:
    rolo01.jsonwrite_contact(data, "ERRORDUMP.json")
    raise