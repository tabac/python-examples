l1 = [1, 2, 3]
d1 = {1: 5, 6: 6}

def f3(n):
    return l1[n] + d1[n]

def f4(n):
    try:
        return f3(n)
    except LookupError as e:
        print("The error is", e)

print(f4(1))
print(f4(6))
print(f4(0))

