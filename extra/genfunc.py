l1 = ["Andersen", "Larsen", "Skov", "Petersen", "Søndergaard"]

def sennavne(l):
    for navn in l:
        if "sen" in navn.lower():
            yield navn

print(sennavne(l1))
for sen in sennavne(l1):
    print(sen)

def firstsennavn(l):
    for navn in l:
        if "sen" in navn.lower():
            return navn

sen = firstsennavn(l1)
print(sen)

def allsennavne(l):
    result = []
    for navn in l:
        if "sen" in navn.lower():
            result.append(navn)
    return result

for sen in allsennavne(l1):
    print(sen)

