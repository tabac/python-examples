import doctest

_cache = {}
def fib(n):
    """Fibonacci function

       Returns the n'th fibonacci number
       n: The n'th number

       >>> fib(0)
       0
       >>> fib(1)
       1
       >>> fib(6)
       8
    """
    if n in _cache:
        return _cache[n]
    if n < 2:
        result = n
    else:
        result = fib(n-1) + fib(n-2)
    _cache[n] = result
    return result

def fac(n):
    """Factorial funcion

       Returns the n'th factorial number
       n: the n'th number
       
       >>> fac(0)
       1
       >>> fac(5)
       120
       >>> fac(6)
       720
    """
    prod = 1
    for i in range(1, n+1):
        prod = prod * i
    return prod

if __name__ == "__main__":
    doctest.testmod()