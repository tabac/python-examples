class SeventeenError(Exception):
    pass

while True:
    try:
        a = int(input("What is the first number : "))
        b = int(input("What is the second number : "))
        if a == 17:
            raise SeventeenError("17 is not a cool number")
        print(a+b, a-b, a*b, a/b)
        break
    except ValueError:
        print("Bad number")
    except ZeroDivisionError:
        print("I can't divide by zero ...")
    except SeventeenError:
        print("I am a bit shy of the number 17 ...")
