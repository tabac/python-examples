class Car:

    def __init__(self, make, model):
        self.passengers = []
        self.make = make
        self.model = model

    def drive(self):
        print("Wrooom, wroooom")

    def __str__(self):
        return "Car({} {})".format(self.make, self.model)

class Truck(Car):

    def __init__(self, make, model, loadsize):
        self.loadsize = loadsize
        self.cargo = []
        super().__init__(make, model)

    def load(self, stuff):
        pass

    def unload(self):
        pass

    def __str__(self):
        return "Truck({} {} {})".format(self.make, self.model, self.loadsize)


c1 = Car("Ford", "Mustang")
t1 = Truck("Ford", "F200", "1 ton")
print(c1)
print(t1)

