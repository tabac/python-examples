import json

def list_contacts(data):
    output = ""
    for p in data:
        output = output + "{:20} {:20} {:10} {:12}\n".format(p['name'], p['email'], p['tel'], p['birthday'])
    return output

def insert_contact(data, name, email, tel, birthday):
    p = {}
    p['name'] = name
    p['email'] = email
    p['tel'] = tel
    p['birthday'] = birthday
    data.append(p)

def find_contact(data, search_name):
    match_list = []
    for p in data:
        if search_name.lower() in p['name'].lower():
            match_list.append(p)
    return match_list

def update_contact(data, search_name, name=None, email=None, tel=None, birthday=None):
    persons = find_contact(data, search_name)
    for p in persons:
        if name is not None:
            p['name'] = name
        if email is not None:
            p['email'] = email
        if tel is not None:
            p['tel'] = tel
        if birthday is not None:
            p['birthday'] = birthday

def delete_contact(data, search_name):
    persons = find_contact(data, search_name)
    for p in persons:
        data.remove(p)

def jsonwrite_contact(data, filename):
    f = open(filename, "w")
    json.dump(data, f, indent=2)
    

def jsonread_contact(data, filename):
    f = open(filename, "r")
    data.clear()
    data.extend(json.load(f))
    print(data)

if __name__ == "__main__":

    data = []
    jsonread_contact(data, "data.json")
    print(data)
    insert_contact(data, 'Charlie Hanson', 'charlie@companý.com', '112233', '1971-04-26')
    persons = find_contact(data, 'charl')
    print(persons)
    update_contact(data, 'ben', tel="321654")
    delete_contact(data, 'andrew')
    print(data)
    jsonwrite_contact(data, "data2.json")
