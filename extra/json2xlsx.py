import json
import xlsxwriter
import pprint
import datetime

with open('data.json') as f:
    data = json.load(f)

pprint.pprint(data)

book = xlsxwriter.Workbook("rolo.xlsx")
sheet = book.add_worksheet("people")
date_format = book.add_format({'num_format': 'd mmmm yyyy'})

row = 0
for p in data:
    date = datetime.datetime.strptime(p['birthday'], "%Y-%m-%d")
    sheet.write_string(row, 0, p['name'])
    sheet.write_string(row, 1, p['email'])
    sheet.write_string(row, 2, p['tel'])
    sheet.write_datetime(row, 3, date, date_format)
    row = row + 1
book.close()
    
