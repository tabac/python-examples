for n in range(1, 101):
    if n % 7 == 0:
        print("boom!", end=", ")
    elif str(n)[0] == "7":
        print("boom!", end=", ")
    elif n >= 10 and str(n)[1] == "7":
        print("boom!", end=", ")
    else:
        print(n, end=", ")
