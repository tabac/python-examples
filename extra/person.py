class Person:
    def __init__(self, name, tel, email, birthday):
        self.name = name
        self.tel = tel
        self.email = email
        self.birthday = birthday

    def __str__(self):
        return "Person: {}, {}, {}, {}".format(self.name, self.tel, self.email, self.birthday)

class PersonList:
    def __init__(self):
        self._list = []

    def add(self, pers):
        self._list.append(pers)

    def __str__(self):
        result = ""
        for pers in self._list:
            result = result + str(pers) + "\n"
        return result


if __name__ == "__main__":
    ole = Person(name="Ole", tel="11223344", email="ole@gmail.com", birthday="1961-09-26")
    print(ole)
    peter = Person(name="Peter", tel="44332211", email="peter@gmail.com", birthday="1967-11-13")
    rolodex = PersonList()
    rolodex.add(ole)
    rolodex.add(peter)
    print(rolodex)
