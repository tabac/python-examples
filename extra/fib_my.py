def fib(n):
    """The fibonacci function

    >>> fib(6)
    8
    >>> fib(7)
    13
    """
    if n < 2:
        return n
    else:
        return fib(n-1) + fib(n-2)

for n in range(38):
    print(n, fib(n))
