import math

def calcprimes(limit):
    candidates = list(range(3, limit+1, 2))
    primes = [2]
    while candidates:
        p = candidates.pop(0)
        primes.append(p)
        if p > math.sqrt(limit):
            primes.extend(candidates)
            return primes
        new_cand = []
        for e in candidates:
            if e % p != 0:
                new_cand.append(e)
        candidates = new_cand
    return primes

pr = calcprimes(1000)
print(pr)
