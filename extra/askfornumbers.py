class DevilishError(Exception):
    pass

while True:
    try:
        a_str = input("What is a : ")
        b_str = input("What is b : ")
        if a_str == "" and b_str == "":
            break
        a = int(a_str)
        b = int(b_str)
        if a == 666 or b == 666:
            raise DevilishError("We don't do this number!")
        print("{} + {} = {}".format(a, b, a+b))
        print("{} - {} = {}".format(a, b, a-b))
        print("{} * {} = {}".format(a, b, a*b))
        print("{} / {} = {}".format(a, b, a/b))
    except ValueError as e:
        print("Hey, this is not a number : ", e)
    except ZeroDivisionError as e:
        print("I can't divide by zero, man!", e)
    except Exception as e:
        print("What is wrong with this ? ", type(e), e)
