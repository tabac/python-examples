class Dog:
    kind = "canine"
    def __init__(self, name, age):
        self.name = name
        self.tricks = []
        # self.age = age
        self._age = age*12

    def __str__(self):
        return "Dog( {} )".format(self.name)

    def add_trick(self, trick):
        self.tricks.append(trick)

    def get_age(self):
        return self._age/12

    def set_age(self, age):
        self._age = age*12

    age = property(get_age, set_age)

class Hunter(Dog):
    def __init__(self, name, age, color):
        # self.name = name
        # self.tricks = []
        # # self.age = age
        # self._age = age*12
        super().__init__(name, age)
        self.color = color


fido = Dog("Fido", 3)
print(fido)
print(Dog.kind)
fido.add_trick("Play dead")
fido.tricks.append("Sit")
print(fido.age)
fido.age = 4
print(fido.get_age())
print(fido.tricks)
rover = Hunter("Rover", 5, "Brown")
print(rover.get_age())
