class Library:
    def __init__(self):
        self.books = []
        self.patrons = []

    def find_book(self, title):
        for bk in self.books:
            if title.lower() in bk.title.lower():
                return bk.handle
        return None

    def borrow_book(self, bookhandle, patronhandle):
        for bk in self.books:
            if bookhandle == bk.handle:
                if bk.status == "available":
                    bk.status = "lentout"
                    bk.lender = patronhandle
                else:
                    raise Exception("Lent out")
                return
        raise Exception("Book Handle not found")               

    def return_book(self, bookhandle):
        for bk in self.books:
            if bookhandle == bk.handle:
                bk.lender = None
                bk.status = "available"        

class Book:
    status_set = {"available", "lentout", "lost", "archived"}
    handle_counter = 1
    def __init__(self, title):
        self.status = "available"
        self.lender = None
        self.title = title
        self.handle = Book.handle_counter
        Book.handle_counter = Book.handle_counter + 1

class Patron:
    handle_counter = 1
    def __init__(self, name):
        self.name = name
        self.handle = Patron.handle_counter
        Patron.handle_counter = Patron.handle_counter + 1

if __name__ == "__main__":
    library = Library()
    patron1 = Patron("Andrew")
    library.patrons.append(patron1)
    book1 = Book("Gone With the Wind")
    library.books.append(book1)
    bh = library.find_book("Gone")
    library.borrow_book(bh, patron1.handle)
    library.return_book(bh)
    print("done")
