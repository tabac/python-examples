def primes(limit):
    candlist = list(range(2, limit+1))
    primelist = []
    while candlist:
        p = candlist.pop(0)
        primelist.append(p)
        # new_candlist = []
        # for c in candlist:
        #     if c % p != 0:
        #         new_candlist.append(c)
        # candlist = new_candlist
        candlist = [c for c in candlist if c % p != 0]
    return primelist

if __name__ == "__main__":
    print(primes(1000))
