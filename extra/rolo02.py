import json

class Person:

    def __init__(self, name, email, tel, birthday):
        self.name = name
        self.email = email
        self.tel = tel
        self.birthday = birthday

    def __str__(self):
        return "Person({})".format(self.name)

    def columnal(self):
        return "{:20} {:20} {:10} {:12}".format(self.name, self.email, self.tel, self.birthday)

class Employee(Person):
    
    def __init__(self, name, email, tel, birthday, manager):
        self.manager = manager
        super().__init__(name, email, tel, birthday)

    def columnal(self):
        return super().columnal() + "{:15}".format(self.manager)

class PersonList:

    def __init__(self):
        self._list = []

    def columnal(self):
        output = ""
        for p in self._list:
            output = output + p.columnal() + "\n"
        return output

    def insert(self, person):
        self._list.append(p)

    def find_contact(self, search_name):
        match_list = PersonList()
        for p in self._list:
            if search_name.lower() in p.name.lower():
                match_list.insert(p)
        return match_list

    def delete_contact(self, person):
        self._list.remove(p)

if __name__ == "__main__":

    data = PersonList()
    p = Person('Charlie Hanson', 'charlie@companý.com', '112233', '1971-04-26')
    data.insert(p)
    p = Person('Charlie Larson', 'larson@companý.com', '112233', '1971-04-26')
    data.insert(p)
    p = Employee('Charlie Larson', 'larson@companý.com', '112233', '1971-04-26', 'Tom Gerrit')
    data.insert(p)
    print(data.columnal())