import facfib as ff

for n in range(101):
    print(n, ff.fac(n), ff.fib(n))
