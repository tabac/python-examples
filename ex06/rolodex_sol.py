import rolo

data = rolo.data

menu = """
    1. List contacts
    2. Add contact
    3. Search for contact
    4. Update contact
    5. Delete contact

    9. Exit
"""

finished = False
while not finished:
    print(menu)
    choice = input("Please select a menu choice : ")
    if choice == '1':
        rolo.list_persons(data)
    elif choice == '2':
        name = input("Name of new contact : ")
        email = input("Email of new contact : ")
        tel = input("Phone number of new contact : ")
        birthday = input("Birthday of new contact : ")
        rolo.insert_person(data, name, email, tel, birthday)
    elif choice == '3':
        search = input("Enter a search string : ")
        p = rolo.find_person(data, search)
        print("Name    :", p['name'])
        print("Email   :", p['email'])
        print("Tel     :", p['tel'])
        print("Birthday:", p['birthday'])
    elif choice == '4':
        search = input("Enter a search string : ")
        name = input("Name (or blank for unchanged) : ")
        email = input("Email (or blank for unchanged) : ")
        tel = input("Phone number (or blank for unchanged) : ")
        birthday = input("Birthday (or blank for unchanged) : ")
        rolo.update_person(data, search, name, email, tel, birthday)
    elif choice == '5':
        search = input("Enter a search string : ")
        rolo.delete_person(data, search)
    elif choice == '9':
        print("Have a nice day ...")
        finished = True
    else:
        print("*** Invalid Choice! ***")

