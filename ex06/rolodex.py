import rolo

data = rolo.data

menu = """
    1. List contacts
    2. Add contact
    3. Search for contact
    4. Update contact
    5. Delete contact

    9. Exit
"""

finished = False
while not finished:
    print(menu)
    choice = input("Please select a menu choice : ")
    if choice == '1':
        pass
    elif choice == '2':
        pass
    elif choice == '3':
        pass
    elif choice == '4':
        pass
    elif choice == '5':
        pass
    elif choice == '9':
        print("Have a nice day ...")
        finished = True
    else:
        print("*** Invalid Choice! ***")

