bool1 = 3 < 5
bool2 = 3 > 5
print(bool1, bool2)

if 0:
    print("Zero")

if 17:
    print("Non-zero")

if "":
    print("Empty")

if "abc":
    print("Non-empty")

if []:
    print("Empty")

if [1, 2, 3]:
    print("Non-empty")
