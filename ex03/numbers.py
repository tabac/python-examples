
decimal1 = 27
decimal2 = -45
decimal3 = 65535
hexa1 = 0x1b
hexa2 = -0x2d
hexa3 = 0xffff
octal1 = 0o33
octal2 = -0o55
octal3 = 0o177777
binary1 = 0b11011
binary2 = -0b101101
binary3 = 0b1111111111111111

print(decimal1, decimal2, decimal3)
print(hexa1, hexa2, hexa3)
print(octal1, octal2, octal3)
print(binary1, binary2, binary3)

float1 = 1.1
float2 = 2.2
float3 = 3.3
print(float1, float2, float3, float1 + float2 - float3)
if float1 + float2 == float3:
    print("Same!")
else:
    print("Different!")

people = 3
apples = 17
print("If {} people share {} apples, they get {} each and {} remains".format(
        people, apples, apples//people, apples%people
    )
)

import math

angle = 60.0
radians = angle/180*math.pi
sinus = math.sin(radians)
cosinus = math.cos(radians)
print(angle, radians, sinus, cosinus)
