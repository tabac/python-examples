text1 = 'abc'
text2 = "abc"
text3 = "abc\"def\n"
text4 = '''This string spans
several lines
of text'''
text5 = "C:\\temp\\newfile.txt"
text6 = r"C:\temp\newfile.txt"
text7 = '"' + r"abc\def" + '"'
text8 = "abc" + "def"
text9 = ("xxxxxxxxxxxxxxxxxxxxxxxxxx"
         + "yyyyyyyyyyyyyyyyyyyyyyyyyy")
text10 = "abc" * 3
text11 = "abcdef"
char1 = text11[3]
char2 = text11[-1]
sl1 = text11[1:3]
sl2 = text11[:3]
sl3 = text11[3:]
sl4 = text11[0:6:2]

print(text1)
print(text2)
print(text3)
print(text4)
print(text5)
print(text6)
print(text7)
print(text8)
print(text9)
print(text10)
print(text11)

print(char1)
print(char2)

print(sl1)
print(sl2)
print(sl3)
print(sl4)
