max_y = 10
max_x = 10
for y in range(1, max_y + 1):
    for x in range(1, max_x + 1):
        outstr = "{:4} ".format(x*y)
        print(outstr, end="")
    print()
