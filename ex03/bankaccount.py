sum = 1000
payment = 50
interest_rate = 0.03
interest_base = 0
year = 2019
month = 11

print("Year Md Sum      Acc Intr")
print("==== == ======== ========")

while sum < 10000:
    print("{:4d} {:2d} {:8.2f} {:8.2f}".format(year, month, sum, interest_base))
    # ...    