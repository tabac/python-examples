def multprint(max_y=10, max_x=10):
    for y in range(1, max_y + 1):
        for x in range(1, max_x + 1):
            if (x*y < 100):
                print(end=" ")
            if (x*y < 10):
                print(end=" ")
            print(x*y, end=" ")
        print()

multprint(max_x=20)
multprint(max_y=5)

def mul2020(max_y=20, max_x=20):
    return multprint(max_y, max_x)
