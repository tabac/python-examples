
"""This example shows the range operator

   range() can do ...
   * max
   * min max
   * min max step
"""

for i in range(10):
    print(i, end=" ")
print()

for i in range(10, 20):
    print(i, end=" ")
print()

for i in range(100, 200, 10):
    print(i, end=" ")
print()
