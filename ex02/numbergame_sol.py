import random

my_num = random.randint(1, 100)
print("I chose a number between 1 and 100 (both inclusive). Can you guess it?")
guessed = False
while not guessed:
    your_answer = input("Your guess : ")
    your_num = int(your_answer)
    if your_num == my_num:
        print("Hooray, you guessed it!")
        guessed = True
        break
    elif your_num > my_num:
        print("Too high, ...")
    elif your_num < my_num:
        print("Too low, ...")
    if abs(your_num - my_num) < 5:
        print("... pretty close")
