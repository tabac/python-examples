num = 1
for line in open("lipsum.txt"):
    line = line.rstrip()
    out = "{:02d}: {}".format(num, line)
    print(out)
    num = num + 1
