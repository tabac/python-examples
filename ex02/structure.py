#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""This is an example of the structure of a Python file.

   The different parts are explained in the slide
"""

import sys

# This is a comment

class MyClass:
    pass

def my_function(param):
    pass

def main(args):
    pass

if __name__ == "__main__":
    main(sys.argv)
