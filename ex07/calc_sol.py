while True:
    try:
        a = int(input("First number : "))
        b = int(input("Second number : "))
        r1 = a + b
        r2 = a - b
        r3 = a * b
        r4 = a / b
        print(a, b, r1, r2, r3, r4)
    except (ValueError, TypeError, ZeroDivisionError):
        print("Your numbers are wrong")
    else:
        break
