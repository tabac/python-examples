class MyError(Exception):
    pass

def fail(n):
    if n == 0:
        raise MyError("n can't be zero")
    else:
        return 1

try:
    fail(1)
    fail(0)
except MyError as e:
    print(e)