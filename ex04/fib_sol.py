_cache = {}
def fib(n):
    """The fibonacci function

    >>> fib(6)
    8
    >>> fib(7)
    13
    """
    if n in _cache:
        return _cache[n]
    elif n < 2:
        _cache[n] = n
        return n
    else:
        _cache[n] = fib(n-1) + fib(n-2)
        return _cache[n]

for n in range(101):
    print(n, fib(n))
