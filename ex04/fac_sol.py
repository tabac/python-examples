def fac(n):
    """Factorial function

    >>> fac(0)
    1
    >>> fac(5)
    120
    """
    prod = 1
    for i in range(1, n+1):
        prod = prod * i
    return prod

for n in range(101):
    print(n, fac(n))
